#!/usr/bin/ruby

def help_msg
  puts <<END
  #{__FILE__} <pkg1> <pkg2> ... <pkgx>

  This command expects one or more package names as arguments. These can also
  be the directories of some checked out package repositories. In this case
  the script works inside the checked out copy and will fix the remote url
  afterwards.

  It will then determine the package versions in stable, oldstable, and
  oldoldstable using the rmadison service, create branches for these releases
  based on tags 'debian/<version>', add debian/.gbp.conf,
  debian/.gitattributes, and debian/salsa-ci.yml in these branches, and fix the
  Vcs fields in debian/control accordingly. The branches are then pushed to the
  repository, and the reposiroty archived (if chosen) and moved into
  ruby-team/attic.

END
  exit
end

help_msg if ARGV.empty?

require_relative './lib/debian'
require_relative './lib/git'
require_relative './lib/salsa'

ARGV.each do |name|
  project=get_project(name)
  next unless project
  if Dir.exists?(name)
    puts "I: Directory #{name} exists."
    g = get_local_repo(name)
  else
    g = clone_repo(name)
  end
  next unless g
  #g.config('push.followTags', 'true')
  g.chdir do
    default_branch=%x[git symbolic-ref --short refs/remotes/origin/HEAD | sed 's@^origin/@@'].strip
    g.branch(default_branch).checkout
  end
  package_versions=get_rmadison_for_package(name)
  if package_versions.empty?
    puts "I #{name}: Does not appear to be in any active Debian release. Consider archiving #{name}."
  else
    package_versions.each { |release, version| create_branch(name, g, release.to_s, version) if release.to_s.match?(/\b(buster|stretch|jessie)/) }
  end
  printf "Archive project #{name} before moving it to ruby-team/attic? ... [Y/N] "
  prompt = STDIN.gets.chomp
  archive_project(project) if prompt.downcase == 'y'
  mv_project_to_attic(project)
  g.set_remote_url(g.remote, g.remote.url.gsub(/(ruby-team\/)/, '\1attic/'))
  puts "I #{g.dir.path}: Set remote #{g.remote} to new url #{g.remote.url}."
end

# vim: set ts=2 sw=2 ai si et:
