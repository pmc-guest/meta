require 'git'
require 'logger'

#Git.configure do |c|
#
#end

def get_local_repo(name)
  puts "I #{name}: Accessing local git copy."
  begin
    g = Git.open(name)
    return g
  rescue
    STDERR.puts "E #{name}: Directory exists and is not a git repository."
    return nil
  end
end

def clone_repo(name)
  puts "I #{name}: Attempting to clone repository."
  begin
    g = Git.clone(["git@salsa.debian.org:ruby-team",name].join("/"), name, :bare => false)
    return g
  rescue Git::GitExecuteError => error
    STDERR.puts error.message
    STDERR.puts "E #{name}: Error checking out git repository."
    return nil
  end
end

def create_branch(name, r, branch, version)
  debian_branch=['debian',branch].join("/")
  debian_version=['debian',version].join("/")
  default_branch=r.current_branch

  puts "I #{r.dir.path}: Attempt to create branch #{branch} for #{version}"
  if r.is_branch?(debian_branch) or r.is_branch?(branch)
    STDERR.puts "E #{r.dir.path}: Branch #{branch} or #{debian_branch} already exists. Returning."
    return
  end

  begin
    puts "I #{r.dir.path}: Looking for #{debian_version} tag"
    r.tag(debian_version)
  rescue => error
    STDERR.puts error.message
    STDERR.puts "E #{r.dir.path}: Version #{debian_version} does not exist. Returning."
    return
  end

  puts "I #{r.dir.path}: Checking out tag #{debian_version}."
  begin
    r.checkout(debian_version)
  rescue => error
    STDERR.puts error.message
    STDERR.puts "E #{r.dir.path}: Error checking out #{debian_version}. Returning."
    return
  end

  puts "I #{r.dir.path}: Creating branch #{debian_branch} from tag #{debian_version}."
  begin
    r.branch(debian_branch).checkout
    r.chdir do

      # first commit
      r.branch(debian_branch).in_branch("Configure CI and GBP for #{branch.capitalize}") do
        break unless Dir.exists?('debian')
        puts "I #{r.dir.path}: Creating salsa CI and GBP configuration files."
        File.open('debian/.gbp.conf', 'w') do |f|
          f.puts <<END
[DEFAULT]
debian-branch = #{debian_branch}
END
        end
        File.open('debian/.gitattributes', 'w') do |f|
          f.puts <<END
.gitattributes export-ignore
gbp.conf export-ignore
salsa-ci.yml export-ignore
END
        end
        File.open('debian/salsa-ci.yml', 'w') do |f|
          f.puts <<END
---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml

variables:
  RELEASE: '#{branch}'
  SALSA_CI_DISABLE_LINTIAN: 1
  SALSA_CI_DISABLE_REPROTEST: 1
END
        end
        r.add(['debian/.gbp.conf', 'debian/.gitattributes', 'debian/salsa-ci.yml'])
      end

      r.branch(debian_branch).in_branch("Fix Vcs-* fields and set correct branch for #{branch.capitalize}") do
        break unless File.exists?('debian/control')
        puts "I #{r.dir.path}: Fixing Vcs* fields in debian/control."
        control = File.read('debian/control')
        if control.match?(/^Vcs-Browser/)
          system("sed -i -e \"/^Vcs-Browser/ c Vcs-Browser: https://salsa.debian.org/ruby-team/attic/#{name}\" debian/control")
        else
          system("sed -i -e \"/^Standards-Version/ a Vcs-Browser: https://salsa.debian.org/ruby-team/attic/#{name}\" debian/control")
        end
        if control.match?(/^Vcs-Git/)
          system("sed -i -e \"/^Vcs-Git/ c Vcs-Git: https://salsa.debian.org/ruby-team/attic/#{name}.git -b #{debian_branch}\" debian/control")
        else
          system("sed -i -e \"/^Standards-Version/ a Vcs-Git: https://salsa.debian.org/ruby-team/attic/#{name}.git -b #{debian_branch}\" debian/control")
        end
        r.add('debian/control')
      end
    end
  rescue => error
    STDERR.puts error.message
    STDERR.puts "E #{r.dir.path}: Something went wrong. Not pushing."
    r.checkout(default_branch)
    return
  end

  puts "I #{r.dir.path}: Pushing branch #{r.current_branch} to #{r.remote}."
  r.config("branch.#{r.current_branch}.remote", r.remote)
  r.config("branch.#{r.current_branch}.merge", "refs/heads/#{r.current_branch}")
  r.config("push.pushOption", "ci.skip")
  r.push(r.remote, r.current_branch)
  r.config("push.pushOption", "")
  r.checkout(default_branch)
end

# vim: set ts=2 sw=2 ai si et:
